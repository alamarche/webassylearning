package main

import (
	"fmt"
	"syscall/js"
	"time"
)

func main() {
	fmt.Println("This is a test")
	block := make(chan bool, 1)

	g := js.Global()
	doc := g.Get("document")
	body := doc.Get("body")

	doc.Set("title", "This is the new title")
	fmt.Println(body.String())
	time.Sleep(3 * time.Second)

	// test := make(map[string]interface{})
	// test["body"] = "new text"
	// doc.Set("body", js.ValueOf(test))

	g.Call("GlobalEventHandlers.onclick")

	<-block

}
