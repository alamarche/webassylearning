module bitbucket.org/alamarche/webassylearning

go 1.14

require (
	github.com/asticode/go-astikit v0.8.0 // indirect
	github.com/asticode/go-astilectron v0.16.0 // indirect
	github.com/dennwc/dom v0.3.0 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/norunners/vert v0.0.0-20190616030331-d9c8c2aacdf1 // indirect
	honnef.co/go/js/dom v0.0.0-20200509013220-d4405f7ab4d8 // indirect
)
